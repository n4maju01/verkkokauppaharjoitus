# Verkkokauppaharjoitus #

http://pitkamatkaeipelota.fi/verkkokauppaharjoitus/

### FI ###

PHP:llä toteutettu Bootstrapia hyödyntävä verkkokauppasivupohja. Sisältää yksinkertaisia perustoimintoja, kuten tuotteiden ja tuoteryhmien lisäämisen, tuotteiden laittamisen ostoskoriin ja tilauksen tekemisen ja tarkastelun.

Tehty harjoitustyönä PHP-kurssilla.

### EN ###

An e-commerce Bootstrap template written in PHP utilizing Bootstrap. Has some very basic functionalities, like adding products and categories, adding items to the cart and making and checking an order.

Done as a school project in a PHP course.

The interface is in Finnish.