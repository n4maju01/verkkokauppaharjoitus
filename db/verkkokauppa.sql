drop database if exists verkkokauppa;

create database if not exists verkkokauppa;

use verkkokauppa;

create table if not exists tuoteryhma (
    id int primary key auto_increment,
    nimi varchar(50) not null unique
);

create table if not exists tuote (
    id int primary key auto_increment,
    nimi varchar(50) not null,
    kuvaus text,
    hinta decimal(5,2) not null,
    kuva varchar(30),
    tuoteryhma_id int not null,
    index (tuoteryhma_id),
    foreign key (tuoteryhma_id) references tuoteryhma(id) on delete restrict
);

create table if not exists asiakas (
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    lahiosoite varchar(50) not null,
    postitoimipaikka varchar(30) not null,
    postinumero varchar(15) not null,
    email varchar(50),
    puhelin varchar(20)
);

create table if not exists tilaus (
    id int primary key auto_increment,
    aika timestamp default current_timestamp,
    asiakas_id int not null,
    index (asiakas_id),
    foreign key (asiakas_id) references asiakas(id) on delete restrict
);

create table if not exists tilausrivi (
    tilaus_id int not null,
    index (tilaus_id),
    foreign key (tilaus_id) references tilaus(id) on delete restrict,
    tuote_id int not null,
    index (tuote_id),
    foreign key (tuote_id) references tuote(id) on delete restrict
);