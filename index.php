<?php include_once 'inc/top.php'; ?>
<?php
if ($_SERVER['REQUEST_METHOD']=='GET') {
    if (isset($_GET['tuoteryhma_id'])) {
        $tuoteryhma_idjm = filter_input(INPUT_GET, "tuoteryhma_id", FILTER_SANITIZE_STRING);
    }
    
    else {
        $tuoteryhma_idjm = 1;
    }
}
?>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-xs-6">      
                <div class="dropdown">
                    <button class="btn btn-default btn-lg dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      Tuotteet
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <?php
                      try {
                            $sqljm = "SELECT id, nimi FROM tuoteryhma";
                            $kyselyjm = $tietokantajm->query($sqljm);
                            $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
                            while ($tietuejm = $kyselyjm->fetch()) {
                                print "<li><a href='" . $_SERVER['PHP_SELF'] . "?tuoteryhma_id=" . $tietuejm->id . "'>" . $tietuejm->nimi . "</a></li>";
                            }
                        }

                        catch (PDOException $pdoex) {
                            print "<div class='col-sm-12 virhe'>";
                            print "Tuoteryhmien haussa tapahtui virhe.</br> " . $pdoex->getMessage();
                            print "</div>";
                        }                
                      ?>
                    </ul>
                </div>       
            </div>
        <?php include_once 'inc/kori.php'; ?>
        </div>             
    </div>
</div>
<div class="row">             
    <div class="col-xs-12 tuotteet">
        <div class="row">
            <div class="col-xs-12">
            <?php
                try {
                    $sqljm = "SELECT nimi FROM tuoteryhma WHERE id=" . $tuoteryhma_idjm;
                    $kyselyjm = $tietokantajm->query($sqljm);
                    $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
                    $tietuejm = $kyselyjm->fetch();
                    
                    print "<h4>" . $tietuejm->nimi . "</h4>";
                    print "<hr>";
                }

                catch (PDOException $pdoex) {
                    print "<div class='col-sm-12 virhe'>";
                    
                    print "Tuoteryhmän haussa tapahtui virhe.</br> " . $pdoex->getMessage();
                    print "</div>";
                }                
            ?>
            </div>
        </div>
        <div class="row">
        <?php
        try {
            $sqljm = "SELECT * FROM tuote WHERE tuoteryhma_id=" . $tuoteryhma_idjm;
            $kyselyjm = $tietokantajm->query($sqljm);
            $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
            while ($tietuejm = $kyselyjm->fetch()) {
                print "<div class='col-xs-6 col-sm-4 col-md-3 tuote'>";
                    print "<p>";
                    print "<a href='nayta.php?tuote_id=" . $tietuejm->id . "'><img src='products/" . $tietuejm->kuva . "'></a>";
                    print "<br />";
                    print $tietuejm->nimi;
                    print "<br />";
                    print $tietuejm->hinta . " €";
                    print "<br />";
                    print "</p>";
                print "</div>";
            }
        }

        catch (PDOException $pdoex) {
            print "<div class='col-sm-12 virhe'>";
            print "Tuotteiden haussa tapahtui virhe.</br> " . $pdoex->getMessage();
            print "</div>";
        }                
        ?>
        </div>       
    </div>
</div>   
<?php include_once 'inc/bottom.php'; ?>