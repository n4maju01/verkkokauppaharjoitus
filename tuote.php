<?php include_once 'inc/top.php'; ?>
<?php include_once 'apu.php'; ?>
<div class="row">             
    <div class="col-xs-12 yllapito">
        <div class="row">
            <div class="col-xs-12">
                <h4>Lisää tuote</h4>
                <hr>
            </div>
        </div>
        <?php
        if ($_SERVER['REQUEST_METHOD']=="POST") {
            try {
                $nimijm=filter_input(INPUT_POST, "nimijm", FILTER_SANITIZE_STRING);
                $kuvausjm=filter_input(INPUT_POST, "kuvausjm", FILTER_SANITIZE_STRING);
                $hintajm=filter_input(INPUT_POST, "hintajm", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                $tuoteryhma_idjm=filter_input(INPUT_POST, "tuoteryhma_idjm", FILTER_SANITIZE_NUMBER_INT);
                $kuvajm = "default.jpg";
                
                if ($_FILES['kuvajm']['error'] == UPLOAD_ERR_OK) {
                    if ($_FILES['kuvajm']['size'] > 0) {
                        $tyyppijm = $_FILES['kuvajm']['type'];
                        if (strcmp($tyyppijm, "image/jpeg") == 0 || 
                                strcmp($tyyppijm, "image/pjpeg") == 0) {
                            $kuvajm = basename($_FILES['kuvajm']['name']);
                            $kansiojm = 'products/';
                            make_thumb($_FILES['kuvajm']['tmp_name'], $kansiojm, 100, $kuvajm);
                        }
                        else {
                            print "<div class='col-sm-12 virhe'>";
                            print "Voit ladata vain jpg-kuvia. Tuotteelle asetettiin oletuskuva.</br>";
                            print "</div>";
                        }
                    }                
                }

                $kyselyjm=$tietokantajm->prepare("INSERT INTO tuote (nimi, kuvaus, kuva, hinta, tuoteryhma_id)" 
                        . "VALUES (:nimijm,:kuvausjm,:kuvajm,:hintajm,:tuoteryhma_idjm)");

                $kyselyjm->bindValue(":nimijm", $nimijm,PDO::PARAM_STR);
                $kyselyjm->bindValue(":kuvausjm", $kuvausjm,PDO::PARAM_STR);
                $kyselyjm->bindValue(":hintajm", $hintajm,PDO::PARAM_STR);
                $kyselyjm->bindValue(":tuoteryhma_idjm", $tuoteryhma_idjm,PDO::PARAM_STR);
                $kyselyjm->bindValue(":kuvajm", $kuvajm,PDO::PARAM_STR);
                $kyselyjm->execute();
                
                print "<div class='col-sm-12 onnistui'>";
                print "Tuote lisätty.";
                print "</div>";
            }

            catch (PDOException $pdoex) {
                print "<div class='col-sm-12 virhe'>";
                print "Tuotteen lisäämisessä tapahtui virhe.</br> " . $pdoex->getMessage();
                print "</div>";
            }
        }
        ?>       
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">
                <form role="form" action="<?php $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Tuoteryhmä</label>
                        <select name="tuoteryhma_idjm" class="form-control" required>
                            <?php
                            try {
                                  $sqljm = "SELECT nimi, id FROM tuoteryhma";
                                  $kyselyjm = $tietokantajm->query($sqljm);
                                  $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
                                  while ($tietuejm = $kyselyjm->fetch()) {
                                      print "<option value='$tietuejm->id'>" . $tietuejm->nimi . "</option>";
                                  }
                              }

                              catch (PDOException $pdoex) {
                                  print "<div class='col-sm-12 virhe'>";
                                  print "Tuoteryhmien haussa tapahtui virhe.</br> " . $pdoex->getMessage();
                                  print "</div>";
                              }                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nimi</label>
                        <input name="nimijm" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Kuvaus</label>
                        <textarea name="kuvausjm" class="form-control" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Kuvatiedosto</label>
                        <input type="file" name="kuvajm" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Hinta</label>
                        <input type="number" min="0" step="0.01" name="hintajm" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Lisää</button>
                    <a href="index.php" class="btn btn-default">Peruuta</a>
                </form>
            </div>
        </div>
    </div>
</div>  
<?php include_once 'inc/bottom.php'; ?>

