<?php include_once 'inc/top.php'; ?>
<?php 
$tuotteetjm = $_SESSION['korijm'];
?>
<div class="row">             
    <div class="col-xs-12 tilaus">
        <div class="row">
            <div class="col-xs-12">
            <?php
            if ($_SERVER['REQUEST_METHOD']=="POST") {
                if (empty($tuotteetjm)) {
                    print "<div class='col-sm-12 virhe'>";
                    print "Ostoskorissa ei ole tuotteita, et voi tilata mitään!";
                    print "</div>";
                }
                else {
                    try { 
                        //Transaktion alku
                        $tietokantajm->beginTransaction();

                        //asiakas
                        $sukunimijm=filter_input(INPUT_POST, "sukunimijm", FILTER_SANITIZE_STRING);
                        $etunimijm=filter_input(INPUT_POST, "etunimijm", FILTER_SANITIZE_STRING);
                        $lahiosoitejm=filter_input(INPUT_POST, "lahiosoitejm", FILTER_SANITIZE_STRING);
                        $postitoimipaikkajm=filter_input(INPUT_POST, "postitoimipaikkajm", FILTER_SANITIZE_STRING);
                        $postinumerojm=filter_input(INPUT_POST, "postinumerojm", FILTER_SANITIZE_STRING);
                        $emailjm=filter_input(INPUT_POST, "emailjm", FILTER_SANITIZE_STRING);
                        $puhelinjm=filter_input(INPUT_POST, "puhelinjm", FILTER_SANITIZE_STRING);

                        $kyselyjm=$tietokantajm->prepare("INSERT INTO asiakas (etunimi, sukunimi,"
                            . " lahiosoite, postitoimipaikka, postinumero, email, puhelin)" 
                            . "VALUES (:etunimijm,:sukunimijm,:lahiosoitejm,:postitoimipaikkajm,"
                            . ":postinumerojm,:emailjm,:puhelinjm)");

                        $kyselyjm->bindValue(":etunimijm", $etunimijm,PDO::PARAM_STR);
                        $kyselyjm->bindValue(":sukunimijm", $sukunimijm,PDO::PARAM_STR);
                        $kyselyjm->bindValue(":lahiosoitejm", $lahiosoitejm,PDO::PARAM_STR);
                        $kyselyjm->bindValue(":postitoimipaikkajm", $postitoimipaikkajm,PDO::PARAM_STR);
                        $kyselyjm->bindValue(":postinumerojm", $postinumerojm,PDO::PARAM_STR);
                        $kyselyjm->bindValue(":emailjm", $emailjm,PDO::PARAM_STR);
                        $kyselyjm->bindValue(":puhelinjm", $puhelinjm,PDO::PARAM_STR);
                        $kyselyjm->execute();
                        
                        //tilaus
                        $asiakas_idjm = $tietokantajm->lastInsertId();

                        $kyselyjm=$tietokantajm->prepare("INSERT INTO tilaus (asiakas_id) "
                            . "VALUES (:asiakas_idjm)");

                        $kyselyjm->bindValue(":asiakas_idjm", $asiakas_idjm,PDO::PARAM_STR);
                        $kyselyjm->execute();
                        
                        //tilausrivi
                        $tilaus_idjm = $tietokantajm->lastInsertId();

                        foreach ($tuotteetjm as $tuotejm) {
                            $tuote_idjm = $tuotejm->id;

                            $kyselyjm=$tietokantajm->prepare("INSERT INTO tilausrivi (tilaus_id, tuote_id) "
                                . "VALUES (:tilaus_idjm,:tuote_idjm)");

                            $kyselyjm->bindValue(":tilaus_idjm", $tilaus_idjm,PDO::PARAM_STR);
                            $kyselyjm->bindValue(":tuote_idjm", $tuote_idjm,PDO::PARAM_STR);
                            $kyselyjm->execute();
                        }

                        //Tallenna muutokset ja tyhjennä kori
                        $tietokantajm->commit();
                        unset($_SESSION['korijm']);

                        print "<div class='col-sm-12 onnistui'>";
                        print "Kiitos tilauksestasi.";
                        print "</div>";
                    }

                    catch (PDOException $pdoex) {
                        //Virhetilanteessa peruuta muutokset
                        $tietokantajm->rollBack();
                        print "<div class='col-sm-12 virhe'>";
                        print "Tilauksessa tapahtui virhe. Tuotteita ei tilattu.</br> " . $pdoex->getMessage();
                        print "</div>";
                    }
                }
                print "<a href='index.php'>Takaisin tuotteisiin.</a>";
            }
            ?>
            <?php if ($_SERVER['REQUEST_METHOD']=="GET") { ?>
                <h4>Tilaus</h4>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">
                <form role="form" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
                    <div class="form-group">
                        <label>Sukunimi</label>
                        <input name="sukunimijm" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Etunimi</label>
                        <input name="etunimijm" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Lähiosoite</label>
                        <input name="lahiosoitejm" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Postitoimipaikka</label>
                        <input name="postitoimipaikkajm" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Postinumero</label>
                        <input name="postinumerojm" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Sähköposti</label>
                        <input name="emailjm" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Puhelin</label>
                        <input name="puhelinjm" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Lähetä</button>
                    <a href="index.php" class="btn btn-default">Peruuta</a>
                </form>
            </div>
        </div>
        <?php } ?>
    </div>
</div>   
<?php include_once 'inc/bottom.php'; ?>