<?php include_once 'inc/top.php'; ?>
<div class="row">             
    <div class="col-xs-12 yllapito">
        <div class="row">
            <div class="col-xs-12">
                <h4>Tilaukset</h4>
                <hr>
            </div>
        </div>      
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">
                <table class="table">
                    <tr>
                        <th>Tilaus</th>
                        <th>Sukunimi</th>
                        <th>Etunimi</th>
                        <th>Aika</th>
                        <th>Tuotteet</th>
                    </tr>
                    <?php 
                    try {
                        $sqljm = "SELECT tilaus_id, sukunimi, etunimi, aika, nimi
                            FROM tuote, asiakas, tilaus, tilausrivi
                            WHERE asiakas.id = tilaus.asiakas_id
                            AND tilaus.id = tilausrivi.tilaus_id
                            AND tilausrivi.tuote_id = tuote.id";
                        $kyselyjm = $tietokantajm->query($sqljm);
                        $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
                        $tulosjm = $kyselyjm->fetchAll();
                        
                        $idjm = 0;
                        foreach ($tulosjm as $tietuejm) {        
                            print "<tr>";
                            if ($idjm == $tietuejm->tilaus_id) { 
                                print "<td></td>";
                                print "<td></td>";
                                print "<td></td>";
                                print "<td></td>";
                            }
                            else {
                                print "<td>" . $tietuejm->tilaus_id . "</td>";
                                print "<td>" . $tietuejm->sukunimi . "</td>";
                                print "<td>" . $tietuejm->etunimi . "</td>";
                                print "<td>" . $tietuejm->aika . "</td>";
                            }
                            print "<td>" . $tietuejm->nimi . "</td>";
                            $idjm =  $tietuejm->tilaus_id;
                            print "</tr>";                      
                        }
                    }

                    catch (PDOException $pdoex) {
                        print "<div class='col-sm-12 virhe'>";
                        print "Tilausten haussa tapahtui virhe.</br> " . $pdoex->getMessage();
                        print "</div>";
                    }   
                    ?>                 
                </table>
            </div>
        </div>
    </div>
</div>  
<?php include_once 'inc/bottom.php'; ?>
