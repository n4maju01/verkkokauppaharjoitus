<?php include_once 'inc/top.php'; ?>
<div class="row">             
    <div class="col-xs-12 yllapito">
        <div class="row">
            <div class="col-xs-12">
                <h4>Lisää tuoteryhmä</h4>
                <hr>
            </div>
        </div>
        <?php
        if ($_SERVER['REQUEST_METHOD']=="POST") {
            try {
                $nimijm=filter_input(INPUT_POST, "nimijm", FILTER_SANITIZE_STRING);     

                $kyselyjm=$tietokantajm->prepare("INSERT INTO tuoteryhma (nimi)" 
                        . "VALUES (:nimijm)");

                $kyselyjm->bindValue(":nimijm", $nimijm,PDO::PARAM_STR);
                $kyselyjm->execute();
                print "<div class='col-sm-12 onnistui'>";
                print "Tuoteryhma tallennettu.";
                print "</div>";
            }

            catch (PDOException $pdoex) {
                print "<div class='col-sm-12 virhe'>";
                print "Tuoteryhmän tallennuksessa tapahtui virhe.</br> " . $pdoex->getMessage();
                print "</div>";
            }
        }
        ?>       
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-6">
                <form role="form" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
                    <div class="form-group">
                        <label>Nimi</label>
                        <input name="nimijm" class="form-control" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Tallenna</button>
                    <a href="index.php" class="btn btn-default">Peruuta</a>
                </form>
            </div>
        </div>
    </div>
</div>  
<?php include_once 'inc/bottom.php'; ?>

