<?php
session_start();
session_regenerate_id();
$tuotteetjm=null;

if (isset($_GET['tyhjenna'])) {
    unset($_SESSION['korijm']);
}

if (!isset($_SESSION['korijm'])) {
    $_SESSION['korijm']=array();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <title></title>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
		</button>        
              <a class="navbar-brand" href="index.php">Verkkokauppa</a>
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                      Ylläpito
                      <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="tuoteryhma.php">Lisää tuoteryhmä</a></li>
                    <li><a href="tuote.php">Lisää tuote</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="tilaukset.php">Tilaukset</a></li>
                    <li><a href="#">Asiakkaat</a></li>
                  </ul>
                </li>
              </ul>
            </div>   
          </div>
        </nav>
        <div class="container content">
            <?php
            try {
                $tietokantajm=new PDO('mysql:host=localhost;dbname=n4maju01_verkkokauppa;charset=utf8', 'n4maju01_admin', 'admin123');
                $tietokantajm->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch (PDOException $pdoex) {
                print "<div class='col-sm-12 virhe'>";
                print "Häiriö järjestelmässä. Tietokantaa ei voida avata.</br> " . $pdoex->getMessage();
                print "</div>";
            }
            ?>