<?php
$tuotteetjm = $_SESSION['korijm'];
$summajm = 0.00;

foreach ($tuotteetjm as $tuotejm) {
    $summajm += $tuotejm->hinta;
}

?>
<div class="col-xs-6" style="text-align: right;">
    <button class="btn btn-lg btn-info" data-toggle="collapse" data-target="#ostoskori">
        <span class="glyphicon glyphicon-shopping-cart ostoskorinappi"><?php printf(" %.2f €", $summajm); ?></span>
    </button>
</div>
<div class="col-xs-12" style="text-align: right;">
    <div id="ostoskori" class="collapse ostoskori">
        <table>
        <?php 
        if (is_array($tuotteetjm)) {
            foreach ($tuotteetjm as $tuotejm) {
                print "<tr>";
                    print "<td>" . $tuotejm->nimi . "</td>";
                    print "<td>" . $tuotejm->hinta . " €</td>";
                    print "<td><span class='glyphicon glyphicon-trash'></span></td>";
                print "</tr>";
            }
            print "<tr class='summa'>";
                print "<td>Summa</td>";
                printf("<td>%.2f €</td>", $summajm);
                print "<td></td>";
            print "</tr>";
        }
        ?>
        </table>   
        <a href="tilaus.php" class="btn btn-primary">
            Kassalle
        </a>
        <button class="btn btn-default" onclick="window.location='index.php?tyhjenna=true';return false;">
            Tyhjennä
        </button>
    </div>
</div>
