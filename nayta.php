<?php include_once 'inc/top.php'; ?>
<div class="row">
    <div class="col-sm-12">       
        <div class="col-xs-6" style="text-align: right;">             
        </div>
        <?php
        if ($_SERVER['REQUEST_METHOD']=="POST") {
            try {
                $tuote_idjm=filter_input(INPUT_POST, "tuote_idjm", FILTER_SANITIZE_STRING);
                $sqljm = "SELECT * FROM tuote WHERE id=" . $tuote_idjm;
                $kyselyjm = $tietokantajm->query($sqljm);
                $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
                $tuotejm = $kyselyjm->fetch();
                $tuotteetjm=$_SESSION['korijm'];
                $tuotteetjm[count($tuotteetjm)]=$tuotejm;
                $_SESSION['korijm']=$tuotteetjm;
            } 

            catch (PDOException $pdoex) {
                print "<div class='col-sm-12 virhe'>";
                print "Tuotteen lisäämisessä ostoskoriin tapahtui virhe.</br> " . $pdoex->getMessage();
                print "</div>";
            }
        }
        ?>
        <?php include_once 'inc/kori.php'; ?>
    </div>
</div>
<div class="row">
<?php    
try {
    $tuote_idjm=filter_input(INPUT_GET, "tuote_id", FILTER_SANITIZE_STRING);
    $sqljm = "SELECT * FROM tuote WHERE id=" . $tuote_idjm;
    $kyselyjm = $tietokantajm->query($sqljm);
    $kyselyjm->setFetchMode(PDO::FETCH_OBJ);
    while ($tietuejm = $kyselyjm->fetch()) {
        print "<div class='col-xs-12'>";
            print "<h4>" . $tietuejm->nimi ."</h4>";
            print "<hr>";
        print "</div>";
        print "<div class='col-xs-3 col-xs-offset-2'>";
            print "<img src='products/" . $tietuejm->kuva . "'>";
        print "</div>";
        print "<div class='col-xs-3'>";
            print "<p>" . $tietuejm->nimi . "&nbsp;&nbsp;" . $tietuejm->hinta . " €</p>";
            print "<p>" . $tietuejm->kuvaus . "</p>";
        print "</div>";
    }
}

catch (PDOException $pdoex) {
    print "<div class='col-sm-12 virhe'>";
    print "Tuotteen haussa tapahtui virhe.</br> " . $pdoex->getMessage();
    print "</div>";
}
?>
    <div class="col-xs-3">
        <form method="post" action="<?php $_SERVER['PHP_SELF'];?>">
            <input type="hidden" name="tuote_idjm" value="<?php print $tuote_idjm ?>">
            <button type="submit" class="btn btn-primary lisaa">Lisää ostoskoriin</button>    
        </form>
        <button type="button" class="btn btn-default takaisin" onclick="window.location='index.php';return false;">Takaisin tuotteisiin</button>
    </div>
</div>
<?php include_once 'inc/bottom.php'; ?>